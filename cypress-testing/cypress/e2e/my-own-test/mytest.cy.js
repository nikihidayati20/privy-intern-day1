/// <reference types="cypress"> />

describe("visit the website", () => {
    it('test-one', () => {
        cy.visit('https://codedamn.com/')

        cy.contains('Learn Programming')
        // cy.contains('start learning').should('exist')

        cy.get('div#root')
        cy.get('div#noroot').should('not.exist')
    })
})